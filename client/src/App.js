import React from 'react';
import './App.css';
import Dashboard from "./components/Dashboard";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Header from "./components/layout/Header";
import AddProjectForm from "./components/project/AddProjectForm";
import {Provider} from "react-redux"; //hook React with Redux
import store from "./store";
import UpdateProjectForm from "./components/project/UpdateProjectForm";
import ProjectBoard from "./components/ProjectBoard";
import AddProjectTaskForm from "./components/projectTasks/AddProjectTaskForm";
import UpdateProjectTask from "./components/projectTasks/UpdateProjectTask";
import Landing from "./components/layout/Landing";
import RegisterForm from "./components/userManagement/RegisterForm";
import LoginForm from "./components/userManagement/LoginForm";
import jwt_decode from "jwt-decode";
import setJWTToken from "./securityUtils/setJWTToken";
import {LOGIN_USER} from "./actions/actionTypes";
import {logOutUser} from "./actions/userActions";
import SecuredRoute from "./securityUtils/SecureRoute";

const jwtToken = localStorage.getItem("jwtToken");

if (jwtToken) {
    setJWTToken(jwtToken);
    const decodedJWTToken = jwt_decode(jwtToken);
    store.dispatch({
        type: LOGIN_USER,
        payload: decodedJWTToken
    });


    if (decodedJWTToken.iat - decodedJWTToken.exp <= 0) {
        store.dispatch(logOutUser());
        window.location.href = "/";
    }
}


function App() {
    return (
        <Provider store={store}>
            <Router>
                <div className="App">
                    <Header/>
                    {/*PUBLIC*/}
                    {/*Landing (LOGIN / REGISTER)*/}
                    <Route exact path="/" component={Landing}/>
                    <Route exact path="/register" component={RegisterForm}/>
                    <Route exact path="/login" component={LoginForm}/>


                    {/*PRIVATE*/}
                    <Switch>
                        {/*Path and Component to bring in*/}
                        <SecuredRoute exact path="/dashboard" component={Dashboard}/>

                        {/*PROJECT && Path Variables*/}
                        <SecuredRoute exact path="/updateProject/:projectId" component={UpdateProjectForm}/>
                        <SecuredRoute exact path="/addProject" component={AddProjectForm}/>SecuredRoute

                        {/*PROJECT_TASKS && Path Variables*/}
                        <SecuredRoute exact path="/projectBoard/:projectId" component={ProjectBoard}/>
                        <SecuredRoute exact path="/addProjectTask/:projectId" component={AddProjectTaskForm}/>
                        <SecuredRoute exact path="/update/:projectId/:projectTaskSequence"
                                      component={UpdateProjectTask}/>
                    </Switch>
                </div>
            </Router>
        </Provider>
    );
}

export default App;
