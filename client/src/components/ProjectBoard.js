import React, {Component} from 'react';
import {Link} from "react-router-dom";
import Backlog from "./Backlog";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {getProjectTasks} from "../actions/projectTaskActions";

class ProjectBoard extends Component {

    componentDidMount() {
        const {projectId} = this.props.match.params;
        this.props.getProjectTasks(projectId);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.errors) {
            this.setState({errors: nextProps.errors})
        }
    }

    render() {
        const {projectId} = this.props.match.params;
        const addProjectTaskLink = "/addProjectTask/" + projectId;
        const {projectTasks} = this.props.backlog;
        const {errors} = this.props;

        const boardAlgorith = (errors, project_tasks) => {
            if (errors.message) {
                return <div className="alert alert-danger text-center">{errors.message}</div>;
            } else {
                return (<div>
                             <div className="alert alert-info text-center">PROJECT TASKS</div>
                                <Link to={addProjectTaskLink}>
                                    <li className="btn btn-primary mb-3">
                                        <i className="fas fa-plus-circle"> Create Project Task</i>
                                    </li>
                                </Link>
                             <Backlog projectTasks={project_tasks}/>
                        </div>)
            }
        };

        return (
            <div className="container">
                {boardAlgorith(errors, projectTasks)}
                <br/>
                <hr/>

            </div>
        );
    }
}

ProjectBoard.propTypes = {
    backlog: PropTypes.object.isRequired,
    getProjectTasks: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired //errors is an Object of the State from redux
};

/**
 * state.projectTask comes from projectTasksReducer?
 */
const mapStateToProps = state => ({
    backlog: state.backlog,
    errors: state.errors
});

/**
 * connect function connects Store to the component.
 */
export default connect(mapStateToProps, {getProjectTasks})(ProjectBoard);