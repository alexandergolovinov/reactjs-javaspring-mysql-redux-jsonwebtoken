import React, {Component} from 'react';
import classnames from "classnames";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {deleteProjectTask} from "../../actions/projectTaskActions";

class ProjectTask extends Component {
    deleteProjectTask = (project_id, project_sequence) => {
        this.props.deleteProjectTask(project_id, project_sequence, this.props.history);
    };

    render() {
        const {projectTask} = this.props;
        const udateOrViewProjectTaskLink = "/update/" + projectTask.projectIdentifier + "/" + projectTask.projectSequence;

        return (
            <div className="card mb-1 bg-light">
                <div className={classnames("card-header", {
                    "text-danger" : projectTask.priority === 1
                })}>
                    <h5>ID: {projectTask.projectSequence} || Priority: {projectTask.priority}</h5>

                </div>
                <div className="card-body bg-light text">
                    <h5 className="card-title">{projectTask.summary}</h5>
                    <p className="card-text text-truncate ">
                        {projectTask.acceptanceCriteria}
                    </p>
                    <Link to={udateOrViewProjectTaskLink} className="btn btn-primary">
                        View / Update
                    </Link>
                    <button onClick={this.deleteProjectTask.bind(this, projectTask.projectIdentifier, projectTask.projectSequence)} className="btn btn-danger ml-4">
                        Delete
                    </button>
                </div>
            </div>
        );
    }
}

ProjectTask.propTypes = {
    deleteProjectTask: PropTypes.func.isRequired
};

export default connect(null, {deleteProjectTask}) (ProjectTask);