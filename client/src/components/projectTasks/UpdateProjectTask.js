import React, {Component} from 'react';
import {updateProjectTask, getProjectTask} from "../../actions/projectTaskActions";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import classnames from "classnames";
import {Link} from "react-router-dom";

class UpdateProjectTask extends Component {
    constructor() {
        super();
        this.state = {
            projectSequence: "",
            summary: "",
            projectIdentifier: "",
            acceptanceCriteria: "",
            status: "",
            priority: 0,
            dueDate: "",
            errors: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        const {projectId} = this.props.match.params;
        const {projectTaskSequence} = this.props.match.params;
        this.props.getProjectTask(projectId, projectTaskSequence);
    }

    onSubmit(event) {
        event.preventDefault();
        const {projectId} = this.props.match.params;
        const {projectTaskSequence} = this.props.match.params;
        // New Object we going to pass to the server
        const updatedProjectTask = {
            id: this.state.id,
            summary: this.state.summary,
            projectSequence: this.state.projectSequence,
            acceptanceCriteria: this.state.acceptanceCriteria,
            status: this.state.status,
            priority: this.state.priority,
            dueDate: this.state.dueDate,
            projectIdentifier: projectId
        };

        this.props.updateProjectTask(updatedProjectTask,projectId,projectTaskSequence, this.props.history);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.projectTask) {
            this.setState({
                id: nextProps.projectTask.id,
                projectSequence: nextProps.projectTask.projectSequence,
                summary: nextProps.projectTask.summary,
                projectIdentifier: nextProps.projectTask.projectIdentifier,
                acceptanceCriteria: nextProps.projectTask.acceptanceCriteria,
                status: nextProps.projectTask.status,
                priority: nextProps.projectTask.priority,
                dueDate: nextProps.projectTask.dueDate,
                errors: nextProps.errors
            })
        }
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        const {projectId} = this.props.match.params;
        const backToProjectBoardLink = "/projectBoard/" + projectId;
        const {errors} = this.state;
        const {projectTask} = this.props;

        const projectTaskFormDisplayLogic = (errors, project_tasks) => {
            if (errors.message) {
                return <div className="alert alert-danger text-center">{errors.message}</div>;
            } else {
                return ( <div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8 m-auto">
                                <Link to={backToProjectBoardLink} className="btn btn-light">
                                    Back to Project Board
                                </Link>
                                <h4 className="display-4 text-center">Update Project Task</h4>
                                <p className="lead text-center">{projectTask.summary} | {projectTask.projectSequence}</p>

                                <form onSubmit={this.onSubmit}>
                                    <div className="form-group">
                                        <input type="text" className={classnames("form-control form-control-lg", {
                                            "is-invalid": errors.summary //this will be added if errors object contains errors.
                                        })}
                                               name="summary"
                                               value={this.state.summary}
                                               onChange={this.onChange} />
                                        {errors.summary && (
                                            <div className="invalid-feedback">{errors.summary}</div>
                                        )}
                                    </div>
                                    <div className="form-group">
                                    <textarea className="form-control form-control-lg" placeholder="Acceptance Criteria"
                                              name="acceptanceCriteria"
                                              value={this.state.acceptanceCriteria}
                                              onChange={this.onChange}></textarea>
                                    </div>
                                    <h6>Due Date</h6>
                                    <div className="form-group">
                                        <input type="date" className="form-control form-control-lg" name="dueDate"
                                               value={this.state.dueDate}
                                               onChange={this.onChange}/>
                                    </div>
                                    <div className="form-group">
                                        <select className="form-control form-control-lg"
                                                name="priority"
                                                value={this.state.priority}
                                                onChange={this.onChange}>

                                            <option value={0}>Select Priority</option>
                                            <option value={1}>High</option>
                                            <option value={2}>Medium</option>
                                            <option value={3}>Low</option>
                                        </select>
                                    </div>

                                    <div className="form-group">
                                        <select className="form-control form-control-lg" name="status"
                                                value={this.state.status}
                                                onChange={this.onChange}>

                                            <option value="">Select Status</option>
                                            <option value="TO_DO">TO DO</option>
                                            <option value="IN_PROGRESS">IN PROGRESS</option>
                                            <option value="DONE">DONE</option>
                                        </select>
                                    </div>

                                    <input type="submit" className="btn btn-primary btn-block mt-4"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>)
            }
        };

        return (
            <div>
                {projectTaskFormDisplayLogic(errors, this.props.projectTask)}
            </div>
        );
    }
}

UpdateProjectTask.propType = {
    projectTask: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    getProjectTask: PropTypes.func.isRequired,
    updateProjectTask: PropTypes.func.isRequired
};

//Map Prop Project Object to State
const mapStateToProps = state => ({
    projectTask: state.backlog.projectTask,
    errors: state.errors
});

export default  connect(mapStateToProps, {updateProjectTask, getProjectTask}) (UpdateProjectTask);