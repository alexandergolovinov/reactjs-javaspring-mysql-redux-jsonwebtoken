import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {createProject} from "../../actions/projectActions";
import classnames from "classnames";

class AddProjectForm extends Component {
    constructor() {
        super();
        this.state = {
            projectName: "",
            projectIdentifier: "",
            description: "",
            startDate: "",
            endDate: "",
            errors: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.errors) {
            this.setState({errors: nextProps.errors})
        }
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    onSubmit(event) {
        event.preventDefault();
        // New Object we going to pass to the server
        const newProject = {
            projectName: this.state.projectName,
            projectIdentifier: this.state.projectIdentifier,
            description: this.state.description,
            startDate: this.state.startDate,
            endDate: this.state.endDate
        };
        this.props.createProject(newProject, this.props.history)
    }

    render() {
        const {errors} = this.state;

        return (
            <div className="project">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h5 className="display-4 text-center">Create Project form</h5>
                            <hr/>
                            <form onSubmit={this.onSubmit}>
                                {/*Wiring projectName input with the server projectName attribute. Should be exact match.*/}
                                {/*SET VALUE to the component state*/}
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.projectName //this will be added if errors object contains errors.
                                    })}
                                           name="projectName"
                                           value={this.state.projectName}
                                           onChange={this.onChange}
                                           placeholder="Project Name"/>
                                    {errors.projectName && (
                                        <div className="invalid-feedback">{errors.projectName}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.projectIdentifier
                                    })}
                                           placeholder="Unique Project ID"
                                           name="projectIdentifier"
                                           onChange={this.onChange}
                                           value={this.state.projectIdentifier}/>
                                    {errors.projectIdentifier && (
                                        <div className="invalid-feedback">{errors.projectIdentifier}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <textarea className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.description
                                    })}
                                              name="description"
                                              value={this.state.description}
                                              onChange={this.onChange}
                                              placeholder="Project Description"></textarea>
                                    {errors.description && (
                                        <div className="invalid-feedback">{errors.description}</div>
                                    )}
                                </div>
                                <h6>Start Date</h6>
                                <div className="form-group">
                                    <input type="date" className="form-control form-control-lg" name="startDate"
                                           value={this.state.startDate} onChange={this.onChange}/>
                                </div>
                                <h6>Estimated End Date</h6>
                                <div className="form-group">
                                    <input type="date" className="form-control form-control-lg" name="endDate"
                                           value={this.state.endDate} onChange={this.onChange}/>
                                </div>

                                <input type="submit" className="btn btn-primary btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

//PropTypes creates declares props
AddProjectForm.propTypes = {
    createProject: PropTypes.func.isRequired, //createProject is function
    errors: PropTypes.object.isRequired //errors is an Object of the State from redux
};

//Connecting Errors Objects to State Errors from Redux
const mapStateToProps = state => ({
    errors: state.errors
});

export default connect(mapStateToProps, {createProject}) (AddProjectForm);