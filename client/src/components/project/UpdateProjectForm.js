import React, {Component} from 'react';
import {getProject, createProject} from "../../actions/projectActions";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import classnames from "classnames";

class UpdateProjectForm extends Component {
    constructor() {
        super();
        this.state = {
            id: "",
            projectName: "",
            projectIdentifier: "",
            description: "",
            startDate: "",
            endDate: "",
            errors: {}
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        const {projectId} = this.props.match.params;
        this.props.getProject(projectId, this.props.history);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.project) {
            this.setState({
                id: nextProps.project.id,
                projectName: nextProps.project.projectName,
                projectIdentifier: nextProps.project.projectIdentifier,
                description: nextProps.project.description,
                startDate: nextProps.project.startDate,
                endDate: nextProps.project.endDate,
                errors: nextProps.errors
            })
        }
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    //The JDBC knows that if you pass ID then you UPDATE!
    onSubmit(event) {
        event.preventDefault();
        // New Object we going to pass to the server
        const updatedProject = {
            id: this.state.id,
            projectName: this.state.projectName,
            projectIdentifier: this.state.projectIdentifier,
            description: this.state.description,
            startDate: this.state.startDate,
            endDate: this.state.endDate
        };
        this.props.createProject(updatedProject, this.props.history)
    }

    render() {
        const {errors} = this.state;
        return (
            <div className="project">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h5 className="display-4 text-center">Create Project form</h5>
                            <hr/>
                            <form onSubmit={this.onSubmit}>
                                {/*Wiring projectName input with the server projectName attribute. Should be exact match.*/}
                                {/*SET VALUE to the component state*/}
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.projectName //this will be added if errors object contains errors.
                                    })}
                                           name="projectName"
                                           value={this.state.projectName}
                                           onChange={this.onChange}
                                           placeholder="Project Name"/>
                                    {errors.projectName && (
                                        <div className="invalid-feedback">{errors.projectName}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control form-control-lg"
                                           placeholder="Unique Project ID"
                                           name="projectIdentifier"
                                           onChange={this.onChange}
                                           value={this.state.projectIdentifier}
                                           disabled/>
                                </div>
                                <div className="form-group">
                                    <textarea className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.description
                                    })}
                                              name="description"
                                              value={this.state.description}
                                              onChange={this.onChange}
                                              placeholder="Project Description"></textarea>
                                    {errors.description && (
                                        <div className="invalid-feedback">{errors.description}</div>
                                    )}
                                </div>
                                <h6>Start Date</h6>
                                <div className="form-group">
                                    <input type="date" className="form-control form-control-lg" name="startDate"
                                           value={this.state.startDate} onChange={this.onChange}/>
                                </div>
                                <h6>Estimated End Date</h6>
                                <div className="form-group">
                                    <input type="date" className="form-control form-control-lg" name="endDate"
                                           value={this.state.endDate} onChange={this.onChange}/>
                                </div>

                                <input type="submit" className="btn btn-primary btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

UpdateProjectForm.propTypes = {
    getProject: PropTypes.func.isRequired,
    createProject: PropTypes.func.isRequired,
    project: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

//Map Prop Project Object to State
const mapStateToProps = state => ({
    project: state.project.project,
    errors: state.errors
});

export default connect(mapStateToProps, {getProject, createProject}) (UpdateProjectForm);