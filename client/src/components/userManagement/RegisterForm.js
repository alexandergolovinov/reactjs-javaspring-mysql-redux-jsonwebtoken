import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import classnames from "classnames";
import {registerUser} from "../../actions/userActions";

class RegisterForm extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            firstname: "",
            lastname: "",
            password: "",
            confirmPassword: "",
            errors: {}
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.errors) {
            this.setState({errors: nextProps.errors})
        }
    }

    onSubmit(event) {
        event.preventDefault();
        // New Object we going to pass to the server
        const newUser = {
            username: this.state.email,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword
        };
        this.props.registerUser(newUser, this.props.history);
    }

    componentDidMount() {
        if (this.props.security.validToken) {
            //IF Token is Valid, then redirect to valid page.
            this.props.history.push("/dashboard");
        }
    }

    render() {
        const {errors} = this.state;

        return (
            <div className="register">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Sign Up</h1>
                            <p className="lead text-center">Create your Account</p>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.firstname
                                    })}
                                           placeholder="Firstname"
                                           name="firstname"
                                           value={this.state.firstname}
                                           onChange={this.onChange}
                                           required/>
                                    {errors.firstname && (
                                        <div className="invalid-feedback">{errors.firstname}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.lastname
                                    })}
                                           placeholder="Lastname"
                                           name="lastname"
                                           value={this.state.lastname}
                                           onChange={this.onChange}
                                           />
                                    {errors.lastname && (
                                        <div className="invalid-feedback">{errors.lastname}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="email" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.message
                                    })}
                                           placeholder="Email Address" name="email"
                                           value={this.state.email}
                                           onChange={this.onChange}/>
                                    {errors.message && (
                                        <div className="invalid-feedback">{errors.message}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="password" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.PasswordMatches
                                    })}
                                           placeholder="Password" name="password"
                                           value={this.state.password}
                                           onChange={this.onChange}/>
                                    {errors.PasswordMatches && (
                                        <div className="invalid-feedback">{errors.PasswordMatches}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="password" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.PasswordMatches
                                    })}
                                           placeholder="Confirm Password"
                                           name="confirmPassword"
                                           value={this.state.confirmPassword}
                                           onChange={this.onChange}/>
                                    {errors.PasswordMatches && (
                                        <div className="invalid-feedback">{errors.PasswordMatches}</div>
                                    )}
                                </div>
                                <input type="submit" className="btn btn-info btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

//Declaring PROPS from Storage in Redux
RegisterForm.propTypes = {
    registerUser: PropTypes.func.isRequired, //createProject is function
    errors: PropTypes.object.isRequired, //errors is an Object of the State from redux
    security: PropTypes.object.isRequired
};

//Connecting Errors Objects to State Errors from Redux
const mapStateToProps = state => ({
    errors: state.errors,
    security: state.security
});

export default connect(mapStateToProps, {registerUser})(RegisterForm);