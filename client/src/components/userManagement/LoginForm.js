import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import classnames from "classnames";
import {loginUser} from "../../actions/userActions";

class LoginForm extends Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            errors: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    onSubmit(event) {
        event.preventDefault();
        // New Object we going to pass to the server
        const loginUser = {
            username: this.state.username,
            password: this.state.password
        };
        this.props.loginUser(loginUser, this.props.history);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.errors) {
            this.setState({errors: nextProps.errors})
        }
    }

    componentDidMount() {
        if (this.props.security.validToken) {
            //IF Token is Valid, then redirect to valid page.
            this.props.history.push("/dashboard");
        }
    }

    render() {
        const {errors} = this.props;
        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Log In</h1>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="email" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.username
                                    })}
                                           onChange={this.onChange}
                                           placeholder="Email Address"
                                           name="username"/>
                                    {errors.username && (
                                        <div className="invalid-feedback">{errors.username}</div>
                                    )}
                                </div>

                                <div className="form-group">
                                    <input type="password" className={classnames("form-control form-control-lg", {
                                        "is-invalid": errors.password
                                    })}
                                           onChange={this.onChange}
                                           placeholder="Password"
                                           name="password"/>
                                    {errors.password && (
                                        <div className="invalid-feedback">{errors.password}</div>
                                    )}
                                </div>
                                <input type="submit" className="btn btn-info btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

//Declaring PROPS from Storage in Redux
LoginForm.propTypes = {
    loginUser: PropTypes.func.isRequired, //createProject is function7
    security: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired, //errors is an Object of the State from redux
};

//Connecting Errors Objects to State Errors from Redux
const mapStateToProps = state => ({
    security: state.security,
    errors: state.errors
});

export default connect(mapStateToProps, {loginUser}) (LoginForm);