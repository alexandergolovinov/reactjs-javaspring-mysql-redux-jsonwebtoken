import React, {Component} from 'react';
import ProjectTask from "./projectTasks/ProjectTask";

function ProjectTasksByFilter({list, filter_name}) {
    return (
        list.filter(pt => pt.status === filter_name)
            .map(pt => {
                return (
                    <ProjectTask key={pt.id} projectTask={pt}/>
                )
            })
    )
}

class Backlog extends Component {
    render() {
        const {projectTasks} = this.props;
        return (
            <div className="container">
                <div className="row">

                    {/*//Col 1*/}
                    <div className="col-md-4">
                        <div className="card text-center mb-2">
                            <div className="card-header bg-secondary text-white">
                                <h3>TO DO</h3>
                            </div>
                        </div>
                        <ProjectTasksByFilter list={projectTasks} filter_name={"TO_DO"}/>
                    </div>

                    {/*//Col 2*/}
                    <div className="col-md-4">
                        <div className="card text-center mb-2">
                            <div className="card-header bg-primary text-white">
                                <h3>In Progress</h3>
                            </div>
                        </div>
                        <ProjectTasksByFilter list={projectTasks} filter_name={"IN_PROGRESS"}/>
                    </div>

                    {/*//Col 3*/}
                    <div className="col-md-4">
                        <div className="card text-center mb-2">
                            <div className="card-header bg-success text-white">
                                <h3>Done</h3>
                            </div>
                        </div>
                        <ProjectTasksByFilter list={projectTasks} filter_name={"DONE"}/>
                    </div>

                </div>
            </div>
        );
    }
}

export default Backlog;