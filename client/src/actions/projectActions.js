import axios from "axios";
import {DELETE_PROJECT, GET_ERRORS, GET_PROJECT, GET_PROJECTS} from "./actionTypes";

/**
 * You can configure the https in package.json - proxy
 */
const ROOT = "http://localhost:8080/api/projects/";
const HTTP_CREATE_NEW_PROJECT = ROOT;
const HTTP_GET_PROJECTS = ROOT;
const HTTP_GET_PROJECT = ROOT;
const HTTP_POST_DELETE_PROJECT = HTTP_GET_PROJECT;


export const createProject = (project, history) => async dispatch => {
    try {
        const response = await axios.post(HTTP_CREATE_NEW_PROJECT, project);
        history.push("/dashboard");
        dispatch({ //action in the reducer
            type: GET_ERRORS, // in this case we will try to take errors from reducer, but the payload will be empty. Happy case.
            payload: {} //set payload data from response and map to state.
        });
    } catch (error) {
        dispatch({ //action in the reducer
            type: GET_ERRORS,
            payload: error.response.data //set payload data from response and map to state.
        });
    }
};

export const getProjects = () => async dispatch => {
    const response = await axios.get(HTTP_GET_PROJECTS);
    dispatch({ //action in the reducer
        type: GET_PROJECTS,
        payload: response.data //set payload data from response and map to state.
    });
};

export const getProject = (id, history ) => async dispatch => {
   try {
       const response = await axios.get(HTTP_GET_PROJECT + id);
       dispatch({ //action in the reducer
           type: GET_PROJECT,
           payload: response.data //set payload data from response and map to state.
       });
   } catch (error) {
       history.push("/dashboard");
   }
};

export const deleteProject = id => async dispatch => {
    if(window.confirm("Are you sure you want to delete the project and all its tasks?")) {
        await axios.delete(HTTP_POST_DELETE_PROJECT + id);
        dispatch({ //action in the reducer
            type: DELETE_PROJECT,
            payload: id
        });
    }
};