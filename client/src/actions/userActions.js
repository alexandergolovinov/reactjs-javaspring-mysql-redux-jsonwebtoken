import axios from "axios";
import {GET_ERRORS, LOGIN_USER} from "./actionTypes";
import setJWTToken from "../securityUtils/setJWTToken";
import jwt_decode from "jwt-decode";


const ROOT = "http://localhost:8080/api/users";
const HTTP_REGISTER_USER = ROOT + "/register";
const HTTP_LOGIN_USER = ROOT + "/login";

export const registerUser = (user, history) => async dispatch => {
    try {
        const response = await axios.post(HTTP_REGISTER_USER, user);
        history.push("/login");
        dispatch({ //action in the reducer
            type: GET_ERRORS, // in this case we will try to take errors from reducer, but the payload will be empty. Happy case.
            payload: {} //set payload data from response and map to state.
        });
    } catch (error) {
        history.push("/register");
        dispatch({ //action in the reducer
            type: GET_ERRORS,
            payload: error.response.data //set payload data from response and map to state.
        });
    }
};

export const loginUser = (userLogin, history) => async dispatch => {
    try {
        //POST Login Request
        const response = await axios.post(HTTP_LOGIN_USER, userLogin);
        //EXTRACT Token from Request
        const {token} = response.data;
        //SAVE Token in LocalStorage (DOM)
        localStorage.setItem("jwtToken", token); //We store token in LocalStorage token
        //SET Token to Header Authorization. Custom Component Action
        setJWTToken(token);
        //DECODE Token
        const decodedToken = jwt_decode(token);

        history.push("/dashboard");

        dispatch({ //action in the reducer
            type: LOGIN_USER,
            payload: decodedToken
        });

    } catch (error) {
        dispatch({ //action in the reducer
            type: GET_ERRORS,
            payload: error.response.data //set payload data from response and map to state.
        });
    }
};

export const logOutUser = () => dispatch => {
    localStorage.removeItem("jwtToken");
    setJWTToken(false);
    dispatch({ //action in the reducer
        type: LOGIN_USER,
        payload: {}
    });
};