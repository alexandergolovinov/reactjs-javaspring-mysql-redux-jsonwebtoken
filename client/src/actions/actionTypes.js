/**
 * Actions are payloads of information that send data from your application to your store.
 They are the only source of information for the store. You send them to the store using store.dispatch().
 */

/**
 * Payload - the actual data.
 */

/**
 * The header identifies the source and destination of the packet
 * Because header information, or overhead data, is only used in the transmission process, it is stripped from the packet when it reaches its destination.
 */
export const GET_ERRORS = "GET_ERRORS";

//Project Task Types
export const GET_PROJECTS = "GET_PROJECTS";
export const GET_PROJECT = "GET_PROJECT";
export const DELETE_PROJECT = "DELETE_PROJECT";

//Project Task Types
export const GET_BACKLOG = "GET_BACKLOG";
export const GET_PROJECT_TASK = "GET_PROJECT_TASK";
export const GET_PROJECT_TASKS = "GET_PROJECT_TASKS";
export const DELETE_PROJECT_TASK = "DELETE_PROJECT_TASK";

//User Types
export const REGISTER_USER = "REGISTER_USER";
export const LOGIN_USER = "LOGIN_USER";