import axios from "axios";
import {
    GET_PROJECT_TASKS,
    GET_ERRORS,
    GET_PROJECT_TASK,
    DELETE_PROJECT,
    DELETE_PROJECT_TASK
} from "../actions/actionTypes";

const ROOT = "http://localhost:8080/api/projecttasks/";
const HTTP_POST_ADD_PROJECT_TASKS = ROOT;
const HTTP_GET_ALL_PROJECT_TASKS = ROOT;
const HTTP_GET_PROJECT_TASK = ROOT + "task/";
const HTTP_POST_UPDATE_PROJECT_TASKS = ROOT + "update-task/";
const HTTP_POST_DELETE_PROJECT_TASK = ROOT + "delete-task/";

export const addProjectTask = (backlog_id, project_task, history) => async dispatch => {
    try {
        await axios.post(HTTP_POST_ADD_PROJECT_TASKS + backlog_id, project_task);
        history.push("/projectBoard/" + backlog_id);
        dispatch({ //action in the reducer
            type: GET_ERRORS, // in this case we will try to take errors from reducer, but the payload will be empty. Happy case.
            payload: {} //set payload data from response and map to state.
        });
    } catch (error) {
        dispatch({ //action in the reducer
            type: GET_ERRORS,
            payload: error.response.data //set payload data from response and map to state.
        });
    }
};

export const getProjectTasks = backlog_id => async dispatch => {
    try {
        const response = await axios.get(HTTP_GET_ALL_PROJECT_TASKS + backlog_id);
        dispatch({ //action in the reducer
            type: GET_PROJECT_TASKS,
            payload: response.data //set payload data from response and map to state.
        });
    } catch (error) {
        dispatch({ //action in the reducer
            type: GET_ERRORS,
            payload: error.response.data //set payload data from response and map to state.
        });
    }
};

export const updateProjectTask = (project_task, backlog_id, project_task_seqeunce, history) => async dispatch => {
    try {
        await axios.post(HTTP_POST_UPDATE_PROJECT_TASKS + backlog_id + "/" + project_task_seqeunce, project_task);
        history.push("/projectBoard/" + backlog_id);
    } catch (error) {
        dispatch({ //action in the reducer
            type: GET_ERRORS,
            payload: error.response.data //set payload data from response and map to state.
        });
    }
};

export const getProjectTask = (backlog_id, project_task_sequence) => async dispatch => {
    try {
        const response = await axios.get(HTTP_GET_PROJECT_TASK + backlog_id + "/" + project_task_sequence);
        dispatch({ //action in the reducer
            type: GET_PROJECT_TASK,
            payload: response.data //set payload data from response and map to state.
        });
    } catch (error) {
        dispatch({ //action in the reducer
            type: GET_ERRORS,
            payload: error.response.data //set payload data from response and map to state.
        });
    }
};

export const deleteProjectTask = (backlog_id, project_task_sequence) => async dispatch => {
    if(window.confirm("Are you sure you want to delete this project task?")) {
        await axios.post(HTTP_POST_DELETE_PROJECT_TASK + backlog_id + "/" + project_task_sequence);
        dispatch({ //action in the reducer
            type: DELETE_PROJECT_TASK,
            payload: project_task_sequence
        });
    }
};