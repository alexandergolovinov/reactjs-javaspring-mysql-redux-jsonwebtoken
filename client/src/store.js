//file for the Redux Management
import  {createStore, applyMiddleware, compose} from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers"; //auto recognizes rootReducer from the file with reducers. A bit confusing. [index.js ]

const initialState = {};
const middleware = [thunk];

let store;

//extension for Chrome DevTools.
if (window.navigator.userAgent.includes("Chrome")) {
    store = createStore(rootReducer, initialState,
        compose(applyMiddleware(...middleware),window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
} else {
    store = createStore(rootReducer, initialState,
        compose(applyMiddleware(...middleware)));
}

export default store;