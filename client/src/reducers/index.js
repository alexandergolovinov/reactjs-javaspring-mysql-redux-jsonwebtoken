import {combineReducers} from "redux";
import errorReducer from "./errorReducer";
import projectReducer from "./projectReducer";
import projectTasksReducer from "./projectTasksReducer";
import userReducer from "./userReducer";

/*Reducers specify how the application's state changes in response to actions sent to the store.
Remember that actions only describe what happened, but don't describe how the application's state changes.*/

export default combineReducers({
    errors: errorReducer,
    project: projectReducer,
    backlog: projectTasksReducer,
    security: userReducer
});