import {LOGIN_USER} from "../actions/actionTypes";

const initialState = {
    user: {},
    validToken: false
};

export default function (state = initialState, action) {

    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                user: action.payload,
                validToken: true
            };
        default:
            return state;
    }
}