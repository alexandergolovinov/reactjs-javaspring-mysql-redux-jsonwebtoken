package ca.intelligence.pmtool.controller;

import ca.intelligence.pmtool.entity.ProjectTask;
import ca.intelligence.pmtool.service.ProjectTaskService;
import ca.intelligence.pmtool.validation.ResponseErrorValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/projecttasks")
@CrossOrigin
public class ProjectTaskController {

    @Autowired
    private ResponseErrorValidation responseErrorValidation;
    @Autowired
    private ProjectTaskService projectTaskService;

    @PostMapping("/{backlogIdentifier}")
    public ResponseEntity<?> addProjectTaskToBacklog(@Valid @RequestBody ProjectTask projectTask,
                                                     BindingResult bindingResult, @PathVariable String backlogIdentifier, Principal principal) {
        ResponseEntity<?> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        ProjectTask projectTask1 = projectTaskService.addProjectTask(backlogIdentifier, projectTask, principal);
        return new ResponseEntity<>(projectTask1, HttpStatus.CREATED);
    }

    @GetMapping("/{backlogIdentifier}")
    public List<ProjectTask> getProjectTasksByBacklog(@PathVariable String backlogIdentifier, Principal principal) {
        return projectTaskService.findProjectTasksByIdentifier(backlogIdentifier, principal);
    }

    @GetMapping("/task/{projectTaskSequence}")
    public ProjectTask getProjectTaskBySequence(@PathVariable String projectTaskSequence) {
        return projectTaskService.findProjectTaskBySequence(projectTaskSequence);
    }

    /**
     * Both parameters should be valid in order to return object from database.
     *
     * @param backlogId           ID of a valid Backlog
     * @param projectTaskSequence ProjectSequence of a ProjectTask
     * @return ProjectTask
     */
    @GetMapping("/task/{backlogId}/{projectTaskSequence}")
    public ResponseEntity<?> getProjectTask(@PathVariable String backlogId, @PathVariable String projectTaskSequence, Principal principal) {
        ProjectTask projectTask = projectTaskService.findPTByBacklogIdAndProjectSequence(backlogId, projectTaskSequence, principal);
        return new ResponseEntity<>(projectTask, HttpStatus.OK);
    }

    @PostMapping(value = "/delete-task/{backlogId}/{projectTaskSequence}")
    public void deleteProjectTask(@PathVariable String backlogId, @PathVariable String projectTaskSequence, Principal principal) {
        projectTaskService.deleteProjectTaskByBacklogAndId(backlogId, projectTaskSequence, principal);
    }

    @PostMapping("/update-task/{backlogId}/{projectTaskSequence}")
    public ResponseEntity<?> updateProjectTask(@PathVariable String backlogId, @PathVariable String projectTaskSequence, Principal principal,
                                               @Valid @RequestBody ProjectTask projectTask, BindingResult bindingResult) {
        ResponseEntity<?> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        ProjectTask projectTask1 = projectTaskService.updateProjectTask(projectTask, backlogId, projectTaskSequence, principal);
        return new ResponseEntity<>(projectTask1, HttpStatus.CREATED);
    }
}