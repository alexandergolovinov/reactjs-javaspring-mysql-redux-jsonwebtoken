package ca.intelligence.pmtool.controller;

import ca.intelligence.pmtool.entity.Project;
import ca.intelligence.pmtool.service.ProjectService;
import ca.intelligence.pmtool.validation.ResponseErrorValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/projects")
@CrossOrigin
public class ProjectController {
    public static final Logger LOG = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectService projectService;
    @Autowired
    private ResponseErrorValidation responseErrorValidation;

    /**
     * Principal is the security object, which stores user's information and JWT
     */
    @PostMapping(value = "") //@Valid annotation makes JSON response more readable and understandable.
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project, BindingResult bindingResult, Principal principal) {
        ResponseEntity<?> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        Project project1 = projectService.saveOrUpdateProject(project, principal.getName());
        return new ResponseEntity<>(project1, HttpStatus.CREATED);
    }

    @GetMapping("/{projectId}")
    public ResponseEntity<?> findProjectById(@PathVariable String projectId, Principal principal) {
        Project project = projectService.findByProjectIdentifierAndUser(projectId, principal);
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @GetMapping(value = "")
    public List<Project> findAllProjects(Principal principal) {
        return projectService.findAllProject(principal);
    }

    @DeleteMapping(value = "/{projectId}")
    public ResponseEntity<?> deleteProjectById(@PathVariable String projectId, Principal principal) {
        projectService.deleteProjectById(projectId, principal);
        return new ResponseEntity<>("Project " + projectId + " was deleted", HttpStatus.OK);
    }
}
