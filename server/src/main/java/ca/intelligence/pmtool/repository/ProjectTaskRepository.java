package ca.intelligence.pmtool.repository;

import ca.intelligence.pmtool.entity.Backlog;
import ca.intelligence.pmtool.entity.ProjectTask;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;


@Repository
public interface ProjectTaskRepository extends CrudRepository<ProjectTask, Long> {

    List<ProjectTask> findByProjectIdentifierOrderByPriority(final String projectIdentifier);

    Optional<ProjectTask> findByProjectSequence(final String string);

    Optional<ProjectTask> findByBacklogAndProjectSequence(final Backlog backlog,final String projectSequence);

}
