package ca.intelligence.pmtool.repository;

import ca.intelligence.pmtool.entity.Backlog;
import ca.intelligence.pmtool.entity.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BacklogRepository extends CrudRepository<Backlog, Long> {

    Optional<Backlog> findByProject(final Project project);
    Optional<Backlog> findByProjectIdentifier(final String projectIdentifier);

}
