package ca.intelligence.pmtool.repository;

import ca.intelligence.pmtool.entity.Project;
import ca.intelligence.pmtool.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    @Override
    Iterable<Project> findAllById(final Iterable<Long> iterable);

    List<Project> findAllByUser(final User user);

    Optional<Project> findByProjectIdentifierAndUser(final String projectIdentifier, final User user);
}
