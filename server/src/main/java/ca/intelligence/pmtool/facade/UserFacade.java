package ca.intelligence.pmtool.facade;

import ca.intelligence.pmtool.dto.UserDTO;
import ca.intelligence.pmtool.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserFacade {

    public User userDTOtoUser(UserDTO userDTO, User user) {
        user.setUsername(userDTO.getUsername());
        user.setFirstname(userDTO.getFirstname());
        user.setLastname(userDTO.getLastname());
        user.setPassword(userDTO.getPassword());
        return user;
    }

}
