package ca.intelligence.pmtool.dto;

import ca.intelligence.pmtool.annotations.PasswordMatches;
import ca.intelligence.pmtool.annotations.ValidEmail;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@PasswordMatches
public class UserDTO {

    @Email(message = "Username needs to be an email")
    @NotBlank(message = "User field is required")
    @Column(unique = true)
    @ValidEmail
    private String username;
    @NotEmpty(message = "Please enter your name")
    private String firstname;
    @NotEmpty(message = "Please enter your lastname")
    private String lastname;
    @NotEmpty(message = "Password is required")
    private String password;
    @Transient //not persistable
    private String confirmPassword;
}
