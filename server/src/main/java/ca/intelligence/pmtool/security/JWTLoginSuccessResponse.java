package ca.intelligence.pmtool.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * When the User Logs In successfully return Response with valid Token
 */

@Setter
@Getter
@AllArgsConstructor
public class JWTLoginSuccessResponse {
    private boolean success;
    private String token;
}
