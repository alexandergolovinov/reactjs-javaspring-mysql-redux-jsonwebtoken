package ca.intelligence.pmtool.service;

import ca.intelligence.pmtool.entity.User;
import ca.intelligence.pmtool.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * This Service is used in SecurityConfig by AuthenticationManagerBuilder
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        return user.orElseGet(user::get);
    }

    @Transactional
    public User loadUserById(Long id){
        Optional<User> user = userRepository.findById(id);
        return user.orElseGet(user::get);
    }
}
