package ca.intelligence.pmtool.service;

import ca.intelligence.pmtool.entity.Backlog;
import ca.intelligence.pmtool.entity.ProjectTask;
import ca.intelligence.pmtool.exceptions.BacklogNotFoundException;
import ca.intelligence.pmtool.exceptions.ProjectTaskNotFoundException;
import ca.intelligence.pmtool.repository.ProjectTaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.security.Principal;
import java.util.List;

@Service
public class ProjectTaskService {
    public static final Logger LOG = LoggerFactory.getLogger(ProjectTaskService.class);

    private ProjectTaskRepository projectTaskRepository;
    private BacklogService backlogService;
    private ProjectService projectService;

    @Autowired
    public ProjectTaskService(ProjectTaskRepository projectTaskRepository, BacklogService backlogService,  ProjectService projectService) {
        this.projectTaskRepository = projectTaskRepository;
        this.backlogService = backlogService;
        this.projectService = projectService;
    }

    public ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask, Principal principal) throws BacklogNotFoundException {
        /*Use projectService, because it already has validation for the USER - PROJECT ownership.
        If PROJECT does not belong to USER*/
        Backlog backlog = projectService.findByProjectIdentifierAndUser(projectIdentifier, principal).getBacklog();

        projectTask.setBacklog(backlog);
        Integer BacklogSequence = backlog.getPtSequence();
        BacklogSequence++;
        backlog.setPtSequence(BacklogSequence);
        projectTask.setProjectSequence(backlog.getProjectIdentifier() + "-" + BacklogSequence);
        projectTask.setProjectIdentifier(projectIdentifier);

        if (ObjectUtils.isEmpty(projectTask.getStatus())) {
            projectTask.setStatus("TO_DO");
        }

        if (ObjectUtils.isEmpty(projectTask.getPriority())) { //In the future we need projectTask.getPriority()== 0 to handle the form
            projectTask.setPriority(3);
        }
        LOG.info("Creating Project Task for Project {}", projectIdentifier);
        return projectTaskRepository.save(projectTask);
    }

    public List<ProjectTask> findProjectTasksByIdentifier(String backlogIdentifier, Principal principal) {
        Backlog backlog = projectService.findByProjectIdentifierAndUser(backlogIdentifier, principal).getBacklog();
        return backlog.getProjectTasks();
    }

    public ProjectTask findProjectTaskBySequence(String projectSequence) {
        return projectTaskRepository.findByProjectSequence(projectSequence).orElseThrow(() -> new ProjectTaskNotFoundException("Project task " + projectSequence + " not found"));
    }

    public ProjectTask findPTByBacklogIdAndProjectSequence(String backlogId, String projectSequence, Principal principal) {
        Backlog backlog = projectService.findByProjectIdentifierAndUser(backlogId, principal).getBacklog();

        List<ProjectTask> projectTaskList = backlog.getProjectTasks();
        return projectTaskList.stream()
                .filter(task -> task.getProjectSequence().equals(projectSequence))
                .findFirst().orElseThrow(() -> new ProjectTaskNotFoundException("Project task " + projectSequence + " not found"));
    }

    public void deleteProjectTaskByBacklogAndId(String backlogId, String projectSequence, Principal principal) {
        Backlog backlog = projectService.findByProjectIdentifierAndUser(backlogId, principal).getBacklog();

        List<ProjectTask> projectTaskList = backlog.getProjectTasks();
        ProjectTask projectTask = projectTaskList.stream()
                .filter(task -> task.getProjectSequence().equals(projectSequence))
                .findFirst().orElseThrow(() -> new ProjectTaskNotFoundException("Project task " + projectSequence + " not found"));

        LOG.info("Deleting Project Task for Project {}", backlogId);
        projectTaskRepository.delete(projectTask);
    }

    public ProjectTask updateProjectTask(ProjectTask updateProjectTask, String backlogId, String projectSequence, Principal principal) {
        Backlog backlog = projectService.findByProjectIdentifierAndUser(backlogId, principal).getBacklog();

        List<ProjectTask> projectTaskList = backlog.getProjectTasks();
        ProjectTask projectTask = projectTaskList.stream()
                .filter(task -> task.getProjectSequence().equals(projectSequence))
                .findFirst().orElseThrow(() -> new ProjectTaskNotFoundException("Project task " + projectSequence + " not found"));

        ProjectTask projectToSave = updateProjectTaskFields(projectTask, updateProjectTask);

        return projectTaskRepository.save(projectToSave);
    }

    /**
     * The method updates the Project Task in database by the object coming from UI
     * @param updatedProjectTask comes from UI form
     * @return currently updated version in DB
     */
    private ProjectTask updateProjectTaskFields(ProjectTask projectTask, ProjectTask updatedProjectTask) {
        if (!StringUtils.isEmpty(updatedProjectTask.getSummary())) {
            projectTask.setSummary(updatedProjectTask.getSummary());
        }

        if (!StringUtils.isEmpty(updatedProjectTask.getAcceptanceCriteria())) {
            projectTask.setAcceptanceCriteria(updatedProjectTask.getAcceptanceCriteria());
        }

        if (!StringUtils.isEmpty(updatedProjectTask.getStatus())) {
            projectTask.setStatus(updatedProjectTask.getStatus());
        }

        if (!StringUtils.isEmpty(updatedProjectTask.getPriority())) {
            projectTask.setPriority(updatedProjectTask.getPriority());
        }

        if (!ObjectUtils.isEmpty(updatedProjectTask.getDueDate())) {
            projectTask.setDueDate(updatedProjectTask.getDueDate());
        }

        return projectTask;
    }

}
