package ca.intelligence.pmtool.service;

import ca.intelligence.pmtool.dto.UserDTO;
import ca.intelligence.pmtool.entity.User;
import ca.intelligence.pmtool.exceptions.UserAlreadyExistException;
import ca.intelligence.pmtool.facade.UserFacade;
import ca.intelligence.pmtool.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    public static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserRepository userRepository;
    private UserFacade userFacade;

    @Autowired
    public UserService(UserRepository userRepository,BCryptPasswordEncoder bCryptPasswordEncoder,  UserFacade userFacade) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userFacade = userFacade;
    }

    public User createNewUser(UserDTO user) {
        User createdUser = new User();
        createdUser = userFacade.userDTOtoUser(user, createdUser);
        createdUser.setPassword(bCryptPasswordEncoder.encode(createdUser.getPassword()));
        LOG.info("Saving new User {} {}", user.getFirstname(), user.getLastname());
        try {
            return userRepository.save(createdUser);
        } catch (Exception e) {
            //PS! Do not make hints that it is the EMAIL, which already exist.
            throw new UserAlreadyExistException("The user: " + createdUser.getUsername() + " already exist. Please check credentials.");
        }
    }
}
