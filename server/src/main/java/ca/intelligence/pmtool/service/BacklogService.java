package ca.intelligence.pmtool.service;

import ca.intelligence.pmtool.entity.Backlog;
import ca.intelligence.pmtool.entity.Project;
import ca.intelligence.pmtool.exceptions.BacklogNotFoundException;
import ca.intelligence.pmtool.repository.BacklogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BacklogService {
    public static final Logger LOG = LoggerFactory.getLogger(BacklogService.class);

    private BacklogRepository backlogRepository;

    @Autowired
    public BacklogService(BacklogRepository backlogRepository) {
        this.backlogRepository = backlogRepository;
    }

    public Backlog createNewBacklog(Project project) {
        LOG.info("Creating new Backlog Object for Project: {}", project.getProjectIdentifier());
        Backlog backlog = new Backlog();
        project.setBacklog(backlog);
        backlog.setProject(project);
        backlog.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
        return backlog;
    }

    public Backlog findBacklogByProject(Project project) {
        Optional<Backlog> backlogOptional = backlogRepository.findByProject(project);
        return backlogOptional.orElse(null);
    }

    public Backlog findByProjectIdentifier(String projectIdentifier) {
        return backlogRepository.findByProjectIdentifier(projectIdentifier).orElseThrow(() -> new BacklogNotFoundException("Backlog with ID: " + projectIdentifier + " Not Found"));
    }
}
