package ca.intelligence.pmtool.service;

import ca.intelligence.pmtool.entity.Project;
import ca.intelligence.pmtool.entity.User;
import ca.intelligence.pmtool.exceptions.ProjectNotFoundException;
import ca.intelligence.pmtool.repository.ProjectRepository;
import ca.intelligence.pmtool.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    public static final Logger LOG = LoggerFactory.getLogger(ProjectService.class);

    private ProjectRepository projectRepository;
    private BacklogService backlogService;
    private UserRepository userRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository, BacklogService backlogService, UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.backlogService = backlogService;
        this.userRepository = userRepository;
    }

    /**
     * If object contains ID then we update it. If ID is empty, then we create a new object.
     * @param project passed from Controller
     * @return
     */
    public Project saveOrUpdateProject(Project project, String username) {
        //It will find a user from the Principal. Otherwise it would not be executed.
        User user = userRepository.findByUsername(username).get();
        project.setUser(user);
        project.setProjectLeader(user.getFirstname() + " " + user.getLastname());

        project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());

        if (project.getId() == null) {
            LOG.info("Creating Project {}", project.getProjectIdentifier());
            backlogService.createNewBacklog(project);
        }

        if (project.getId() != null) {
            LOG.info("Updating Project {}", project.getProjectIdentifier());
            project.setBacklog(backlogService.findBacklogByProject(project));
        }
        return projectRepository.save(project);
    }

    public Project findByProjectIdentifierAndUser (String projectId, Principal principal) {
        Optional<User> user = userRepository.findByUsername(principal.getName());
        return user.map(value -> projectRepository.findByProjectIdentifierAndUser(projectId, value).orElseThrow(() ->
                new ProjectNotFoundException("Project " + projectId + " Not Found for User " + value.getUserFullname()))).orElse(null);
    }

    public List<Project> findAllProject(Principal principal) {
        Optional<User> user = userRepository.findByUsername(principal.getName());
        return user.map(usr -> projectRepository.findAllByUser(usr)).orElse(null);
    }

    public void deleteProjectById(String projectId, Principal principal) {
        projectRepository.delete(findByProjectIdentifierAndUser(projectId, principal));
    }
}
