package ca.intelligence.pmtool.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
public class Backlog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer ptSequence = 0;
    private String projectIdentifier;
    /**
     * FetchType.EAGER - Loads Automatically right away
     * FetchType.LAZY - Loads on Request.
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROJECT_ID", nullable = false)
    private Project project;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH, mappedBy = "backlog", orphanRemoval = true)
    @JsonIgnoreProperties("backlog") //avoid infinite recursion.
    private List<ProjectTask> projectTasks = new ArrayList<>();
}
